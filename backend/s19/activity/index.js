console.log("Hello World");


            let firstName = "Cherry Mae";
            console.log("First Name: " + firstName);

            let lastName = "Catapan";
            console.log("Last Name: " + lastName);

            let age = 27;
            console.log("Age: " + age);

            let hobbies = ["singing", "dancing", "painting"];
            console.log("Hobbies: ");
            console.log(hobbies);

            let workAddress = {
                houseNumber: '208',
                street: 'Poblacion',
                city: 'Ipil',
                state: 'Zamboanga Sibugay'
            };

            console.log("Work Address: ");
            console.log(workAddress);



    let fullName = "Steve Rogers";
    console.log("My full name is: " + fullName);

    let currentAge = 40;
    console.log("My current age is: " + currentAge);
    
    let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
    console.log("My friends are: ");
    console.log(friends);


    let profile = {

        username: 'captain_america',
        fullName: 'Steve Rogers',
        age: 40,
        isActive: false,

    };
    console.log("My Full Profile: ");
    console.log(profile);
    

    let fullName2 = "Bucky Barnes";
    console.log("My bestfriend is: " + fullName2);

    const lastLocation = "Arctic Ocean";
    
    console.log("I was found frozen in: " + lastLocation);
