//Parameters and Arguments

function printName(name){
	console.log("My name is " + name);
}


//Calling or Invoking our Function "printName"
//Argument --> Invoke

printName("Juana");
printName("Cherry");

let sampleVariable = "John";

printName(sampleVariable);

// Check Divisibility

function checkDivisibility(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibility(64);

// Functions as Arguments

function argumentFunction(){
	console.log("This function was passed as an argument.");
};


function invokeFunction(argument){
	argumentFunction();
};

invokeFunction();


//Multiple Parameters in a Function

function createFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

//Multiple Arguments in an Invocation

createFullName("Juan", "Dela", "Cruz");
createFullName("Juan", "Dela")
createFullName("Juan", "Dela", "Cruz", "Hello");


//Function wit Alert Message
// function showSampleAlert(){
// 	alert("Hello User");
// }

// showSampleAlert();

// console.log("Testing");

//Promt()

// let samplePrompt = prompt("Enter Your Name");

// console.log("Hello" + samplePrompt);

//Function with prompts

function printWelcomeMessage(){
	let firstName = prompt("Enter your First Name");
	let lastName = prompt("Enter your Last Name");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my Page!");
}

printWelcomeMessage();

function greeting(){
	return "Hello, This is the return statement"
}

let greetingFunction = greeting();
console.log(greetingFunction);

console.log(greeting());

//code below return statement will never work.

console.log("Hello this is me");