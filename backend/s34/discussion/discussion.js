// CRUD Operations (MongoDB)

// CRUD Operations are the heart of any backend app.
// Mastering CRUD Operations is essential for any developers.

/*

C - Create/Insert
R - Retrieve/Read
U - Update
D - Delete

*/

//[SECTION] Creating documents

/*

SYNTAX:

db.users.insertOne({object})

*/



db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JS", "Phython"],
	department: "none"
});

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "00000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

db.users.insertOne({
	firstName: "Cherry Mae",
	lastName: "Catapan",
	age: 27,
	contact: {
		phone: "09552110656",
		email: "cherrymaecatapan3@gmail.com"
	},
	courses: ["CSS", "JS", "HTML"],
	department: "none"
});


db.users.insertMany([
{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "09123456789",
		email: "stephenhawking@gmail.com"
	},
	courses: ["PHP", "React", "Python"],
	department: "none"
},
{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "09123456789",
		email: "neilarmstrong@gmail.com"
	},
	courses: ["React", "Laravel", "Sass"],
	department: "none"
}
]);

// [SECTION] READ/RETRIEVE

/*

SYNTAX:

db.users.findOne();
db.users.findOne({field: value})


*/

db.users.findOne();

db.users.findOne({firstName: "Stephen"});


/*

SYNTAX:

db.users.find();
db.users.find({field: value})


*/

db.users.find({department: "none"});

// multiple criteria

db.users.find({department: "none", age: 82});

// [SECTION] Updating Data

/*

SYNTAX:

db.users.updateOne();
db.users.updateOne({criteria}, {$set: {field: value}})


*/

db.users.updateOne(
	{firstName: "Test"}, 
	{$set: {
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "12345",
			email: "billgates@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "active"
	}
});

//find the data

db.users.find({firstName: "Bill"});

db.users.find({firstName: "Test"});

// getting an object inside an object

db.users.find({"contact.email": "billgates@gmail.com"});

//Update many collections

/*

SYNTAX:

db.users.updateMany({criteria}, {$set: {field: value}})


*/

db.users.updateMany(
	{department: "none"}, 
	{$set: {
		department: "HR"}
});


//[SECTION] Deleting a Data

db.users.insert({
	firstName: "Test"
});

/*

SYNTAX:

db.users.deleteOne({criteria})


*/

db.users.deleteOne({
	firstName: "Test"
});

//delete many

db.users.insert({
	firstName: "Bill"
});

/*

SYNTAX:

db.users.deleteMany({criteria})


*/

db.users.deleteMany({
	firstName: "Bill"
});