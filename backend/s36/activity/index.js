
async function fruitsOnSale(db) {
    return await(

            db.fruits.aggregate([
                {
                    $match: {onSale: true}
                },
                {
                    $count: "fruitsOnSale"
                }
            ])



        );
};


async function fruitsInStock(db) {
    return await(
        db.fruits.aggregate([
            {
                $match: {stock:{$gte: 20}}
            },
            {
                $count: "enoughStock"
            }
        ])
            

        );
};


async function fruitsAvePrice(db) {
    return await(

        db.fruits.aggregate([
            {
                $match: {onSale: true}
            },
            {
                $group: {
                    _id: "$supplier_id",
                    avg_price: { $avg: "$price" }
                }
            },
            {
                $sort: { _id: 1 } 
            }
        ])

        );
};



async function fruitsHighPrice(db) {
    return await(
        
        db.fruits.aggregate([
            {
                $group: {
                    _id: "$supplier_id",
                    max_price: {$max: "$price"}
    
               }
            }
        ])
      
       
       
        );
};




async function fruitsLowPrice(db) {
    return await(

        db.fruits.aggregate([
            {
                $group: {
                    _id: "$supplier_id",
                    max_price: {$min: "$price"}
    
               }
            }
        ])

        );
}


try{
    module.exports = {
        fruitsOnSale,
        fruitsInStock,
        fruitsAvePrice,
        fruitsHighPrice,
        fruitsLowPrice
    };
} catch(err){

};